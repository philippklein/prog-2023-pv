# GrundProg 2023: Praesenzveranstaltung

## Zeit

22. Juli 2023

Start: 10:00

Ende: 18:00

## Ablauf

* 10:00: Kickoff
* 10:12: [01.md](01.md)
* 11:12: Besprechung [01.md](01.md)
* 12:00: [02.md](02.md) und Mittagspause
* 14:00: Besprechung [02.md](02.md)
* 15:20: [aufgaben_pw.pdf](aufgaben_pw.pdf)
* 17:00: Besprechung [aufgaben_pw.pdf](aufgaben_pw.pdf)